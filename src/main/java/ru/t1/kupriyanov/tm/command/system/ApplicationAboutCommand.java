package ru.t1.kupriyanov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Anton Kupriyanov");
        System.out.println("E-mail: ankupr29@gmail.com");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show info about programmer.";
    }

}
