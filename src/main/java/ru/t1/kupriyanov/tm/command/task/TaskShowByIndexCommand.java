package ru.t1.kupriyanov.tm.command.task;

import ru.t1.kupriyanov.tm.model.Task;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        final Task task = getTaskService().findOneByIndex(userId, index);
        showTask(task);
    }

    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public String getDescription() {
        return "Show task by index.";
    }

}
