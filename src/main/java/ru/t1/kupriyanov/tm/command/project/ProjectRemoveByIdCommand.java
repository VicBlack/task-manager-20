package ru.t1.kupriyanov.tm.command.project;

import ru.t1.kupriyanov.tm.model.Project;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Project project = getProjectService().findOneById(userId, id);
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

}
