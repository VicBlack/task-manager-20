package ru.t1.kupriyanov.tm.command.system;

import ru.t1.kupriyanov.tm.util.FormatUtil;

public final class ApplicationSystemInfoCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + FormatUtil.formatBytes(usageMemory));
    }

    @Override
    public String getName() {
        return "info";
    }

    @Override
    public String getArgument() {
        return "-i";
    }

    @Override
    public String getDescription() {
        return "Show system information.";
    }
}
