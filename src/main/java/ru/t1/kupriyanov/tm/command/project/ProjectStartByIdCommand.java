package ru.t1.kupriyanov.tm.command.project;

import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return "project-start-by-id";
    }

    @Override
    public String getDescription() {
        return "Start project by id.";
    }

}
