package ru.t1.kupriyanov.tm.command.user;

import ru.t1.kupriyanov.tm.enumerated.Role;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    private final String NAME = "update-user-profile";

    private final String DESCRIPTION = "update user's profile";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE USER'S PROFILE]");
        System.out.println("FIRST NAME: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME: ");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME: ");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
