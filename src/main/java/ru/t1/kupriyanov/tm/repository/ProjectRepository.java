package ru.t1.kupriyanov.tm.repository;

import ru.t1.kupriyanov.tm.api.repository.IProjectRepository;
import ru.t1.kupriyanov.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

}
