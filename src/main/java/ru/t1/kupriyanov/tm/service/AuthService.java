package ru.t1.kupriyanov.tm.service;

import ru.t1.kupriyanov.tm.api.service.IAuthService;
import ru.t1.kupriyanov.tm.api.service.IUserService;
import ru.t1.kupriyanov.tm.enumerated.Role;
import ru.t1.kupriyanov.tm.exception.user.LoginEmptyException;
import ru.t1.kupriyanov.tm.exception.user.AccessDeniedException;
import ru.t1.kupriyanov.tm.exception.user.PasswordEmptyException;
import ru.t1.kupriyanov.tm.exception.user.PermissionException;
import ru.t1.kupriyanov.tm.model.User;
import ru.t1.kupriyanov.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService extends AbstractService<User, IUserService> implements IAuthService {

    //private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        super(userService);
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return repository.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = repository.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = repository.findById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
