package ru.t1.kupriyanov.tm.api.repository;

import ru.t1.kupriyanov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

}