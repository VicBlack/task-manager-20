package ru.t1.kupriyanov.tm.api.service;

import ru.t1.kupriyanov.tm.api.repository.IUserOwnedRepository;
import ru.t1.kupriyanov.tm.enumerated.Sort;
import ru.t1.kupriyanov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    void clear(String userId);

    int getSize(String userId);

    boolean existsById(String userId, String id);

}
