package ru.t1.kupriyanov.tm.exception.user;

public class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email is already used!");
    }

}
