package ru.t1.kupriyanov.tm.exception.system;

import ru.t1.kupriyanov.tm.exception.AbstractException;

public class AbstractSystemException extends AbstractException {

    public AbstractSystemException() {
    }

    public AbstractSystemException(final String command) {
        super(command);
    }

    public AbstractSystemException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractSystemException(final Throwable cause) {
        super(cause);
    }

    public AbstractSystemException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
